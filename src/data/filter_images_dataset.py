import click
import logging
import os
import shutil
import pandas as pd
from dotenv import find_dotenv, load_dotenv
from pathlib import Path


@click.command()
@click.argument('initial_dataset_dir', type=click.Path(exists=True))
@click.argument('dataset_description_filepath',
                type=click.Path(exists=True))
@click.argument('final_dataset_dir', type=click.Path())
def main(initial_dataset_dir,
         dataset_description_filepath,
         final_dataset_dir):
    df = pd.read_csv(dataset_description_filepath, sep='\t')

    if not initial_dataset_dir.endswith('/'):
        initial_dataset_dir += '/'
    if not final_dataset_dir.endswith('/'):
        final_dataset_dir += '/'

    if not os.path.exists(final_dataset_dir):
        os.makedirs(final_dataset_dir)

    for index, row in df.iterrows():
        src_img_path = initial_dataset_dir + row['album_cover_local_name']
        dst_img_path = final_dataset_dir + row['album_cover_local_name']

        shutil.copyfile(src_img_path, dst_img_path)


if __name__ == '__main__':
    log_fmt = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
    logging.basicConfig(level=logging.INFO, format=log_fmt)

    # not used in this stub but often useful for finding various files
    project_dir = Path(__file__).resolve().parents[2]

    # find .env automagically by walking up directories until it's found, then
    # load up the .env entries as environment variables
    load_dotenv(find_dotenv())

    main()
