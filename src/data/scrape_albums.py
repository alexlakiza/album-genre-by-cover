import pickle
import random
import time
import urllib.request

import deezer as de

client = de.Client(headers={'Accept-Language': 'en'})

albums = {}

with open("artists_folder/final_list_artists.pkl", "rb") as f:
    artists_dict = pickle.load(f)

genres = ['Jazz', 'Pop', 'Rap/Hip Hop', 'Alternative', 'Rock', 'Electro']

with open("../data/new_album_covers.csv", mode='w') as csv_file:
    csv_file.write(
        "album_title\talbum_artist\talbum_cover_link\talbum_cover_local_name\t"
        "album_genres\tdeezer_artist_genre\trecord_type\n")

i = 0

cur_genre = ""

for deezer_genre in genres:  # For each genre

    cur_genre = deezer_genre

    for artist in artists_dict[deezer_genre]:  # For each artist of genre

        print(artist)

        current_artist_albums_list = list(
            artist.get_albums())  # Collecting all artist's albums

        if len(current_artist_albums_list) > 16:
            current_artist_albums_list = random.sample(
                current_artist_albums_list, 16)

        time.sleep(0.5)

        for album in list(
                current_artist_albums_list):  # Looping through all
            # albums of artist

            album_dict = album.as_dict()  # Working with dict is better
            # than built-in deezer-python type

            gen_list = [genre.name for genre in album.genres]

            album_dict['genres'] = gen_list  # Adding genres of album to dict

            time.sleep(0.5)

            # Leaving only albums and EP, dumping singles
            proper_album_types = ['album', 'ep']
            # if album does not have any genre, we skip it
            if album_dict['record_type'].lower() in proper_album_types and \
                    len(album_dict['genres']) != 0:

                try:
                    cur_filename = f"{i}.jpg"
                    # Saving album's cover
                    urllib.request.urlretrieve(album_dict['cover_medium'],
                                               cur_filename)
                    print(f"{album_dict['title']} | {cur_filename}")

                    time.sleep(0.5)

                    # Collecting all necessary metadata about album
                    album_dictionary = {'album_title': album_dict['title'],
                                        'album_artist': artist.name,
                                        'album_cover_link': album_dict[
                                            'cover_medium'],
                                        'album_cover_local_name': cur_filename,
                                        'album_genres': album_dict['genres'],
                                        'deezer_artist_genre': deezer_genre,
                                        'record_type': album_dict[
                                            'record_type']}

                    i += 1
                    time.sleep(0.25)

                    with open("../data/new_album_covers.csv",
                              mode='a') as csv_file:
                        csv_file.write(
                            f"{album_dict['title']}\t{artist.name}\t"
                            f"{album_dict['cover_medium']}\t"
                            f"{cur_filename}\t{album_dict['genres']}\t"
                            f"{deezer_genre}\t{album_dict['record_type']}\n")

                except TypeError:
                    print('no')
                    pass

            else:
                pass

with open(f"../data/new_albums_from_deezer_{cur_genre}.pkl", "wb") as f:
    # save pickle of metadata of albums
    pickle.dump(albums, f, protocol=pickle.HIGHEST_PROTOCOL)
