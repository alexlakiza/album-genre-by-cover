import click
import logging
import pandas as pd
from dotenv import find_dotenv, load_dotenv
from pathlib import Path


@click.command()
@click.argument('input_filepath', type=click.Path(exists=True))
@click.argument('output_filepath', type=click.Path())
def main(input_filepath,
         output_filepath):
    df = pd.read_csv(input_filepath, sep='\t')

    df['main_genre'] = df['album_genres'].apply(lambda x: eval(x)[0])
    df.drop(df[~df['main_genre'].isin(
        ['Jazz', 'Pop', 'Rap/Hip Hop', 'Alternative', 'Rock',
         'Electro'])].index, inplace=True)

    df.to_csv(output_filepath, sep='\t')


if __name__ == '__main__':
    log_fmt = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
    logging.basicConfig(level=logging.INFO, format=log_fmt)

    # not used in this stub but often useful for finding various files
    project_dir = Path(__file__).resolve().parents[2]

    # find .env automagically by walking up directories until it's found, then
    # load up the .env entries as environment variables
    load_dotenv(find_dotenv())

    main()
