import os
import warnings
from copy import deepcopy
from datetime import datetime

import mlflow
import pandas as pd
import torch
import torchvision.models as models
import typer
from dotenv import load_dotenv
from mlflow.models import infer_signature
from skimage import io, color
from sklearn.model_selection import train_test_split
from torch.utils.data import Dataset, DataLoader
from torchmetrics import Accuracy
from torchvision.transforms import transforms
from typing_extensions import Annotated

load_dotenv()
remote_server_uri = os.getenv("MLFLOW_TRACKING_URI")
mlflow.set_tracking_uri(remote_server_uri)

warnings.filterwarnings("ignore")
warnings.simplefilter(action='ignore', category=FutureWarning)

df = pd.read_csv('data/interim/final_album_covers.csv', sep='\t')
train_df, val_df = train_test_split(df,
                                    train_size=0.8,
                                    random_state=13)

train_df = train_df.reset_index().drop(['index'], axis=1)
val_df = val_df.reset_index().drop(['index'], axis=1)


class GenresDataset(Dataset):

    def __init__(self, df_slice, transform=None):
        if df_slice == 'train':
            self.data = train_df
        elif df_slice == 'val':
            self.data = val_df
        else:
            self.data = df

        self.class_map = {
            "Rock": 0,
            "Jazz": 1,
            "Electro": 2,
            "Alternative": 3,
            "Rap/Hip Hop": 4,
            "Pop": 5
        }

        self.transform = transform

    def __len__(self):
        # len of our dataset
        return len(self.data)

    def __getitem__(self, idx):
        # magic func to get image and its class
        images_path = "data/processed/"

        # path_to_image and name of image's class
        img_path = images_path + self.data.loc[idx, "album_cover_local_name"]
        class_name = self.data.loc[idx, "main_genre"]

        # read image pixels
        image = io.imread(img_path)

        if len(image.shape) == 2:
            image = color.gray2rgb(image)

        # read image's class and transform into tensor
        class_id = self.class_map[class_name]
        class_id = torch.tensor([class_id])

        if self.transform:
            image = self.transform(image)

        return image, class_id


# Define the transformations to apply to the images
transform = transforms.Compose([
    transforms.ToTensor(),
    transforms.Resize(224),
    transforms.Normalize(mean=[0.485, 0.456, 0.406],
                         std=[0.229, 0.224, 0.225])
])

# Load the train and validation datasets
train_dataset = GenresDataset(df_slice='train', transform=transform)
test_dataset = GenresDataset(df_slice='val', transform=transform)

train_dataloader = DataLoader(train_dataset, batch_size=32, shuffle=True)
test_dataloader = DataLoader(test_dataset, batch_size=32, shuffle=False)

# Load the pre-trained ResNet-18 model
models_dict = {"resnet18": models.resnet18(pretrained=True),
               "AlexNet": models.alexnet(pretrained=True)}


def train_model(model,
                model_name,
                train_loader,
                val_loader,
                criterion,
                optimizer,
                num_epochs,
                metric_fn,
                device):
    validation_accuracies = []
    validation_losses = []

    # Train the model for the specified number of epochs
    for epoch in range(num_epochs):
        # Set the model to train mode
        model.to(device)
        model.train()

        # Initialize the running loss and accuracy
        running_loss = 0.0
        running_corrects = 0

        # Iterate over the batches of the train loader
        for inputs, labels in train_loader:
            # Move the inputs and labels to the device
            inputs = inputs.to(torch.float32).to(device)
            labels = labels.to(device)

            # Zero the optimizer gradients
            optimizer.zero_grad()

            # Forward pass
            outputs = model(inputs)
            _, preds = torch.max(outputs, 1)
            # print(f"{outputs=}")
            # print(f"{labels.reshape(32)}")
            loss = criterion(outputs, labels.reshape(-1))

            # Backward pass and optimizer step
            loss.backward()
            optimizer.step()

            # Update the running loss and accuracy
            running_loss += loss.item() * inputs.size(0)
            running_corrects += torch.sum(preds == labels.data)

        # Calculate the train loss and accuracy
        train_loss = running_loss / len(train_dataset)
        train_acc = running_corrects.float() / len(train_dataset)

        # Set the model to evaluation mode
        model.eval()

        # Initialize the running loss and accuracy
        running_loss = 0.0
        running_corrects = 0

        # Iterate over the batches of the validation loader
        with torch.no_grad():
            for inputs, labels in val_loader:
                # Move the inputs and labels to the device
                inputs = inputs.to(torch.float32).to(device)
                labels = labels.to(device)

                # Forward pass
                outputs = model(inputs)
                _, preds = torch.max(outputs, 1)

                accuracy = metric_fn(preds, labels.flatten())
                mlflow.log_metric("val_accuracy", f"{accuracy:3f}",
                                  step=epoch)

                loss = criterion(outputs, labels.reshape(-1))

                # Update the running loss and accuracy
                running_loss += loss.item() * inputs.size(0)
                running_corrects += torch.sum(preds == labels.data)

        # Calculate the validation loss and accuracy
        val_loss = running_loss / len(test_dataset)
        val_acc = running_corrects.float() / len(test_dataset)

        # Print the epoch results
        print('Epoch [{}/{}], train loss: {:.4f}, train acc: '
              '{:.4f}, val loss: {:.4f}, val acc: {:.4f}'
              .format(epoch + 1, num_epochs, train_loss,
                      train_acc, val_loss, val_acc))
        validation_accuracies.append(val_acc)
        validation_losses.append(val_loss)

    best_model_state = deepcopy(model.state_dict())
    path_to_save_model = f"models/{model_name}_" \
                         f"{datetime.now().strftime('%Y%m%d_%H%M%S')}.pt"
    torch.save(best_model_state, path_to_save_model)
    return model, validation_accuracies, validation_losses


def main(model_name: Annotated[str,
                               typer.Option(help="model name")] = "resnet18",
         n_epochs: Annotated[int, typer.Option(help="num of epochs")] = 10):
    with mlflow.start_run():
        mlflow.get_artifact_uri()

        model = models_dict[model_name]

        for param in model.parameters():
            param.requires_grad = False

        # Modify the last layer of the model
        num_classes = 6  # replace with the number of classes in your dataset
        model.fc = torch.nn.Linear(model.fc.in_features, num_classes)

        criterion = torch.nn.CrossEntropyLoss()
        optimizer = torch.optim.SGD(model.fc.parameters(),
                                    lr=0.001,
                                    momentum=0.9)

        params = {"model_type": model_name,
                  "n_epochs": n_epochs,
                  "optimizer": optimizer,
                  "bathc_size": 16}

        mlflow.log_params(params)

        if torch.backends.mps.is_available():
            device = torch.device("mps")
        else:
            device = torch.device("cpu")

        metric_fn = Accuracy(task="multiclass", num_classes=6).to(device)

        model, val_acc, val_losses = train_model(model=model,
                                                 model_name=model_name,
                                                 train_loader=train_dataloader,
                                                 val_loader=test_dataloader,
                                                 criterion=criterion,
                                                 optimizer=optimizer,
                                                 num_epochs=n_epochs,
                                                 metric_fn=metric_fn,
                                                 device=device)

        batch_example = next(iter(test_dataloader))[0]
        with torch.no_grad():
            inputs = batch_example.to(torch.float32).to(device)
            outputs = model(inputs)
            _, preds = torch.max(outputs, 1)

        signature = infer_signature(batch_example.cpu().numpy(),
                                    preds.cpu().numpy())

        mlflow.pytorch.log_model(
            pytorch_model=model.cpu(),
            artifact_path="model",
            registered_model_name=model_name,
            signature=signature
        )

    experiment = dict(mlflow.get_experiment_by_name("Default"))
    experiment_id = experiment['experiment_id']
    mlflow_df = mlflow.search_runs([experiment_id])
    best_run_id = mlflow_df.loc[0, 'run_id']
    print(best_run_id)


if __name__ == "__main__":
    typer.run(main)
